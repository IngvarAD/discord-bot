#!/bin/python3
import os
import discord
import random
import re
from discord.ext import commands

from dotenv import load_dotenv


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
SERVER = os.getenv('DISCORD_SERVER')
PREFIX = '$'
random.seed(12^(10^23))

bot = commands.Bot(command_prefix=PREFIX)

def roller(dices):
    regex = re.search("^[0-9]*k[0-9]+[\+|\-]{0,1}[0-9]*$", dices)
    result = []
    mod_sign = ''
    modifier = 0

    if regex == None:
        return 'Zły format kości'
    dices = dices.split('k')

    if dices[0] == '':
        rolls_num = 1
    else:
        rolls_num = int(dices[0])

    if '+' in dices[1]:
        mod_sign = '+'
    elif '-' in dices[1]:
        mod_sign = '-'
    if mod_sign != '':
        dices = dices[1].split(mod_sign)
    else:
        dices = [dices[1]]

    if len(dices) == 2:
        modifier = int(dices[1])

    dice_sides = int(dices[0])

    for roll in range(rolls_num):
        result.append(random.randint(1,dice_sides))
    end_sum = sum(result)

    if mod_sign == '+':
        end_sum += modifier
    if mod_sign == '-':
        end_sum -= modifier

    return f'Wynik: {end_sum}\nModyfikator to: {mod_sign}{modifier}\nKolejne rzuty: {result}'

@bot.event
async def on_ready():
    print(f'{bot.user} Connected')

@bot.command(name='pomoc', help='Wyświetla pomoc po polsku')
async def help(ctx):
    help_message = f'Cześć! jestem {bot.user.name}\nReaguję na komendy zaczynające się od: \'{PREFIX}\'\nTeraz tylko {PREFIX}pomoc i {PREFIX}turlaj działają. Przepraszam.'
    await ctx.send(help_message)

@bot.command(name='turlaj', help='Turlacz kostek (np. 2k3+4)')
async def roll(ctx, dices='k2'):
    message = roller(dices)
    await ctx.send(message)


bot.run(TOKEN)
